
public class CarrinhoDeCompras {
	static int valorTotal = 0;
	
	public double retornarValor(Pizza pizza){
		if(pizza.ingrediente.size() == 0) {
			System.out.println("N�o � poss�vel adicionar pizza sem ingrediente");
			return 0;
		}
		pizza.quantidadeIngrediente(pizza);
		valorTotal += pizza.getPreco();
		return valorTotal;
	}
	
	public double getValorTotal() {
		return valorTotal;
	}
	
	

}
