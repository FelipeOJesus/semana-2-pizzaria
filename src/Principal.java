
public class Principal {

	public static void main(String[] args) {
		Pizza p1 = new Pizza();
		Pizza p2 = new Pizza();
		Pizza p3 = new Pizza();
		CarrinhoDeCompras carrinho = new CarrinhoDeCompras();
		
		p1.adicionarIngrediente("Queijo");
		p1.adicionarIngrediente("Calabresa");
		p1.adicionarIngrediente("Cebola");
		
		p2.adicionarIngrediente("Queijo");
		p2.adicionarIngrediente("Presunto");
		p2.adicionarIngrediente("Ovo");
		p2.adicionarIngrediente("Milho");
		p2.adicionarIngrediente("Ervilha");
		p2.adicionarIngrediente("Azeitona");
				
		p3.adicionarIngrediente("Frango");
		p3.adicionarIngrediente("Catupiry");
		
		carrinho.retornarValor(p1);
		carrinho.retornarValor(p2);
		carrinho.retornarValor(p3);
		
		System.out.println("Valor Total: " + carrinho.getValorTotal());
		
		Pizza.contabilizarIngrediente();
	}

}
